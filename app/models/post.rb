class Post < ActiveRecord::Base
	has_many :comments, dependent: :destroy
	belongs_to :user
	validates_presence_of :title, :body
	validates_length_of :title, :in=> 5..30, :message=> "Titulo inválido"
	validates :body, length: {
		minimum: 10,
		maximum: 50,
		too_short: "El texto del post es demasiado corto (minimo %{count} palabras)",
		too_long: "El texto del post es demasiado largo (maximo %{count} palabras)"
	}
end
